package com.gitee.easyopen.register;

import org.springframework.context.ApplicationContext;

import com.gitee.easyopen.ApiConfig;

public interface Initializer {
    void init(ApplicationContext applicationContext, ApiConfig apiConfig);
}
