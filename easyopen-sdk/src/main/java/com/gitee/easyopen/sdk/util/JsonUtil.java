package com.gitee.easyopen.sdk.util;

import com.alibaba.fastjson.JSON;

public class JsonUtil {
    
    /**
     * 对象转json
     * @param obj
     * @return
     */
    public static String toJson(Object obj) {
        return JSON.toJSONString(obj);
    }

    /**
     * json转对象
     * @param json
     * @param clazz
     * @return
     */
    public static <T> T parseOject(String json, Class<T> clazz) {
        return JSON.parseObject(json, clazz);
    }
}
