package com.gitee.easyopen.sdk;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Test;

import com.alibaba.fastjson.JSON;
import com.gitee.easyopen.sdk.param.GoodsParam;
import com.gitee.easyopen.sdk.req.GoodsReq;
import com.gitee.easyopen.sdk.resp.GoodsResp;

public class LimitTest extends BaseTest {

    // 模拟表单重复提交
    @Test
    public void testLimit() throws InterruptedException {
        int threadsCount = 20;
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        final CountDownLatch count = new CountDownLatch(threadsCount);
        final AtomicInteger success = new AtomicInteger();
        for (int i = 0; i < threadsCount; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        countDownLatch.await(); // 等在这里，执行countDownLatch.countDown();集体触发
                        // 业务方法
                        doBusiness(Thread.currentThread().getName());
                        success.incrementAndGet();
                    } catch (Exception e) {
                    } finally {
                        count.countDown();
                    }
                }
            }).start();
        }
        countDownLatch.countDown();
        count.await();
        System.out.println("成功次数：" + success);
    }

    private void doBusiness(String thead) throws IOException {
        GoodsParam param = new GoodsParam();
        param.setGoods_name("iphone6");
        GoodsReq req = new GoodsReq("doc.param.1", param);

        GoodsResp result = client.request(req);

        if (result.isSuccess()) {
            System.out.println("成功(" + thead + "):" + result.getData());
        } else {
            System.err.println("失败(" + thead + "):" + JSON.toJSONString(result));
            throw new RuntimeException(result.getMsg());
        }
    }

}
