package com.gitee.easyopen.sdk;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Test;

import com.gitee.easyopen.sdk.modal.Goods;
import com.gitee.easyopen.sdk.param.GoodsParam;
import com.gitee.easyopen.sdk.req.GoodsReq;
import com.gitee.easyopen.sdk.resp.GoodsResp;
import com.gitee.easyopen.sdk.util.PostUtil;

public class LockTest extends BaseTest {

    // 模拟表单重复提交
    @Test
    public void testLock() throws InterruptedException {
        int threadsCount = 20;
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        final CountDownLatch count = new CountDownLatch(threadsCount);
        final AtomicInteger success = new AtomicInteger();
        for (int i = 0; i < threadsCount; i++) {
            final int j = i;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        countDownLatch.await(); // 等在这里，执行countDownLatch.countDown();集体触发
                        // 业务方法
                        doBusiness(j);
                        success.incrementAndGet();
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        count.countDown();
                    }
                }
            }).start();
        }
        countDownLatch.countDown();
        count.await();
        System.out.println("成功次数：" + success);
    }
    
    private void doBusiness(int i) throws IOException {
        String jwt = PostUtil.postText("http://localhost:8080/jwtLogin", "");
        System.out.println("jwt:" + jwt);

        GoodsParam param = new GoodsParam();
        param.setGoods_name("iphone6 " + i);
        
        GoodsReq req = new GoodsReq("userlock.test", param);
        
        GoodsResp result = client.requestWithJwt(req, jwt);
        
        if (result.isSuccess()) {
            Goods goods = result.getBody();
            System.out.println(goods);
        } else {
            throw new RuntimeException(result.getMsg());
        }
    }
    
}
