package com.gitee.easyopen;

public interface HasConfig {
    void setApiConfig(ApiConfig apiConfig);
    ApiConfig getApiConfig();
}
