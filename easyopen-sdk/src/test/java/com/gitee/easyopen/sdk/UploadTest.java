package com.gitee.easyopen.sdk;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.gitee.easyopen.sdk.param.GoodsParam;
import com.gitee.easyopen.sdk.req.GoodsReq;
import com.gitee.easyopen.sdk.resp.GoodsResp;

public class UploadTest extends BaseTest {

    /**
     * 上传文件，读取本地文件
     * 
     * @throws IOException
     */
    @Test
    public void testUpload() throws IOException {
        GoodsParam param = new GoodsParam();
        param.setGoods_name("iphone6");
        GoodsReq req = new GoodsReq("file.upload", param);

        String path = this.getClass().getResource("").getPath();

        List<UploadFile> files = new ArrayList<>();

        files.add(new UploadFile("headImg", new File(path + "1.txt")));
        files.add(new UploadFile("idcardImg", new File(path + "2.txt")));

        GoodsResp result = client.requestFile(req, files);

        System.out.println("--------------------");
        if (result.isSuccess()) {
            System.out.println(result.getData());
        } else {
            System.out.println("errorMsg:" + result.getMsg());
        }
        System.out.println("--------------------");
    }

    /**
     * 上传文件，直接传文件流。因为有些文件不会落地，但文件最终形式还是byte[]
     * 
     * @throws IOException
     */
    @Test
    public void testUpload2() throws IOException {
        GoodsParam param = new GoodsParam();
        param.setGoods_name("iphone6");
        GoodsReq req = new GoodsReq("file.upload2", param);

        List<UploadFile> files = new ArrayList<>();

        files.add(new UploadFile("headImg","headImg.txt", this.getClass().getResourceAsStream("1.txt")));
        files.add(new UploadFile("idcardImg", "idcardImg.txt", this.getClass().getResourceAsStream("2.txt")));

        GoodsResp result = client.requestFile(req, files);

        System.out.println("--------------------");
        if (result.isSuccess()) {
            System.out.println(result.getData());
        } else {
            System.out.println("errorMsg:" + result.getMsg());
        }
        System.out.println("--------------------");
    }
    
    @Test
    public void testUpload3() throws IOException {
        GoodsParam param = new GoodsParam();
        param.setGoods_name("iphone6");
        GoodsReq req = new GoodsReq("file.upload3", param);

        List<UploadFile> files = new ArrayList<>();

//        files.add(new UploadFile("headImg","headImg.txt", this.getClass().getResourceAsStream("1.txt")));
        files.add(new UploadFile("idcardImg", "idcardImg.txt", this.getClass().getResourceAsStream("2.txt")));

        GoodsResp result = client.requestFile(req, files);

        System.out.println("--------------------");
        if (result.isSuccess()) {
            System.out.println(result.getData());
        } else {
            System.out.println("errorMsg:" + result.getMsg());
        }
        System.out.println("--------------------");
    }

}
