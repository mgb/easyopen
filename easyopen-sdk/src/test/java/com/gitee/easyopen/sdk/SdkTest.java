package com.gitee.easyopen.sdk;

import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gitee.easyopen.sdk.modal.Goods;
import com.gitee.easyopen.sdk.param.GoodsParam;
import com.gitee.easyopen.sdk.req.GoodsReq;
import com.gitee.easyopen.sdk.req.HelloReq;
import com.gitee.easyopen.sdk.resp.GoodsResp;
import com.gitee.easyopen.sdk.resp.HelloResp;
import com.gitee.easyopen.sdk.util.PostUtil;

public class SdkTest extends BaseTest {
    
    @Test
    public void testHello() throws Exception {
        HelloReq req = new HelloReq("hello"); // hello对应@Api中的name属性，即接口名称

        HelloResp result = client.request(req); // 发送请求
        if (result.isSuccess()) {
            String resp = result.getBody();
            System.out.println(resp); // 返回hello world
        } else {
            throw new RuntimeException(result.getMsg());
        }

    }

    @Test
    public void testGet() throws Exception {
        GoodsParam param = new GoodsParam();
        param.setGoods_name("iphone6");
        GoodsReq req = new GoodsReq("goods.get", param);

        GoodsResp result = client.request(req);
        
        System.out.println("--------------------");
        if (result.isSuccess()) {
            Goods goods = result.getBody();
            System.out.println(goods);
        } else {
            System.out.println("errorMsg:" + result.getMsg());
        }
        System.out.println("--------------------");
    }
    
    @Test
    public void testGet2() throws Exception {

        GoodsParam param = new GoodsParam();
        //param.setGoods_name("iphone6");
        GoodsReq req = new GoodsReq("goods.get", param);

        GoodsResp result = client.request(req);
        
        System.out.println("--------------------");
        if (result.isSuccess()) {
            Goods goods = result.getBody();
            System.out.println(goods);
        } else {
            System.out.println("errorMsg:" + result.getMsg());
        }
        System.out.println("--------------------");
    }
    
    // 先浏览器访问http://localhost:8080/go_oauth2 获取accessToken
    @Test
    public void testGetToken() throws Exception {
        String accessToken = "52f306460c034968f517e72c30aa3eee";

        GoodsParam param = new GoodsParam();
        param.setGoods_name("iphone6");
        
        GoodsReq req = new GoodsReq("user.goods.get", param);
        req.setAccess_token(accessToken);
        
        GoodsResp result = client.request(req);
        
        System.out.println("--------------------");
        if (result.isSuccess()) {
            Goods goods = result.getBody();
            System.out.println(goods);
        } else {
            throw new RuntimeException(result.getMsg());
        }
        System.out.println("--------------------");

    }
    
    @Test
    public void testGetWithJwt() throws Exception {
        OpenClient client2 = new OpenClient(url, appKey, secret);
        String jwt = PostUtil.postText("http://localhost:8080/jwtLogin", "");
        System.out.println("jwt:" + jwt);

        GoodsParam param = new GoodsParam();
        param.setGoods_name("iphone6");
        
        GoodsReq req = new GoodsReq("userjwt.goods.get", param);
        
        GoodsResp result = client2.requestWithJwt(req, jwt);
        
        System.out.println("--------------------");
        if (result.isSuccess()) {
            Goods goods = result.getBody();
            System.out.println(goods);
        } else {
            throw new RuntimeException(result.getMsg());
        }
        System.out.println("--------------------");

    }
    
    @Test
    public void testAdd() throws Exception {
        JSONObject param = new JSONObject();

        GoodsParam goods = new GoodsParam();
        goods.setGoods_name("333333333333333333333333333333333333");

        param.put("name", "goods.add");
        param.put("data", URLEncoder.encode(JSON.toJSONString(goods), "UTF-8"));
        param.put("version", "");

        System.out.println("--------------------");
        System.out.println("请求内容:" + JSON.toJSONString(param));
        
        String resp = PostUtil.postString(url, param);

        System.out.println(resp);
        System.out.println("--------------------");
    }
    
    /**
     * 客户端服务端交互的数据都经过加密处理
     * @throws Exception
     */
    @Test
    public void testRsa() throws Exception {
        client = new EncryptClient(url, appKey);
        GoodsParam param = new GoodsParam();
        param.setGoods_name("iphone6");
        GoodsReq req = new GoodsReq("goods.get", param);

        GoodsResp result = client.request(req);
        
        System.out.println("--------------------");
        if (result.isSuccess()) {
            Goods goods = result.getBody();
            System.out.println(goods);
        } else {
            System.out.println("errorMsg:" + result.getMsg());
        }
        System.out.println("--------------------");
    }
    
    @Test
    public void testUpload() throws IOException {
        GoodsParam param = new GoodsParam();
        param.setGoods_name("iphone6");
        GoodsReq req = new GoodsReq("file.upload", param);

        String path = this.getClass().getResource("").getPath();

        List<UploadFile> files = new ArrayList<>();

        files.add(new UploadFile("headImg", new File(path + "1.txt")));
        files.add(new UploadFile("idcardImg", new File(path + "2.txt")));

        GoodsResp result = client.requestFile(req, files);

        System.out.println("--------------------");
        if (result.isSuccess()) {
            System.out.println(result.getData());
        } else {
            System.out.println("errorMsg:" + result.getMsg());
        }
        System.out.println("--------------------");
    }
    
    @Test
    public void testGetMethod() throws Exception {
        OpenClient client2 = new OpenClient(url, appKey, secret);
        String jwt = PostUtil.postText("http://localhost:8080/jwtLogin", "");
        System.out.println("jwt:" + jwt);

        GoodsParam param = new GoodsParam();
        param.setGoods_name("iphone6");
        
        GoodsReq req = new GoodsReq("userjwt.goods.get", param);
        
        GoodsResp result = client2.requestGet(req, jwt);
        
        System.out.println("--------------------");
        if (result.isSuccess()) {
            Goods goods = result.getBody();
            System.out.println(goods);
        } else {
            throw new RuntimeException(result.getMsg());
        }
        System.out.println("--------------------");
    } 

}
