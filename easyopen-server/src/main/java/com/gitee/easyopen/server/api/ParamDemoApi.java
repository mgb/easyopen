package com.gitee.easyopen.server.api;

import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gitee.easyopen.annotation.Api;
import com.gitee.easyopen.annotation.ApiService;
import com.gitee.easyopen.doc.DataType;
import com.gitee.easyopen.doc.annotation.ApiDoc;
import com.gitee.easyopen.doc.annotation.ApiDocField;
import com.gitee.easyopen.doc.annotation.ApiDocMethod;
import com.gitee.easyopen.server.api.param.GoodsParam;

@ApiService
@ApiDoc("参数类型demo")
public class ParamDemoApi {

    @Api(name = "param.type.1")
    @ApiDocMethod(description = "参数类型，自定义类")
    public String demo1(GoodsParam param) {
        return JSON.toJSONString(param);
    }
    
    @Api(name = "param.type.2")
    @ApiDocMethod(description = "参数类型，JSONObject",params = { 
            @ApiDocField(name = "id", dataType = DataType.INT,example="1"),
            @ApiDocField(name = "goodsName", dataType = DataType.STRING, description = "商品名称",example="iPhone6"), 
    })
    public String demo2(JSONObject json) {
        return json.toJSONString();
    }
    
    @Api(name = "param.type.3")
    @ApiDocMethod(description = "参数类型，Map接收",params = { 
            @ApiDocField(name = "id", dataType = DataType.INT,example="1"),
            @ApiDocField(name = "goodsName", dataType = DataType.STRING, description = "商品名称",example="iPhone6"), 
    })
    public String demo3(Map<String, Object> param) {
        return JSON.toJSONString(param);
    }
    
    @Api(name = "param.type.4")
    @ApiDocMethod(description = "参数类型，String接收",params = { 
            @ApiDocField(name = "id", dataType = DataType.INT,example="1"),
            @ApiDocField(name = "goodsName", dataType = DataType.STRING, description = "商品名称",example="iPhone6"), 
    })
    public String demo3(String param) {
        return param;
    }
    
}
