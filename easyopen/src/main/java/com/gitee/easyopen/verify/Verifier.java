package com.gitee.easyopen.verify;

import com.gitee.easyopen.ApiParam;

public interface Verifier {

    boolean verify(ApiParam apiParam, String secret);
}
