package com.gitee.easyopen.sdk;

import org.junit.Test;

import com.alibaba.fastjson.JSON;
import com.gitee.easyopen.sdk.modal.Goods;
import com.gitee.easyopen.sdk.param.GoodsParam;
import com.gitee.easyopen.sdk.req.GoodsReq;
import com.gitee.easyopen.sdk.resp.GoodsResp;

public class RsaTest extends BaseTest {
    
    /**
     * 客户端服务端交互的数据都经过加密处理
     * @throws Exception
     */
    @Test
    public void testGet() throws Exception {
        client = new EncryptClient(url, appKey);
        GoodsParam param = new GoodsParam();
        param.setGoods_name("iphone6");
        GoodsReq req = new GoodsReq("goods.get", param);

        GoodsResp result = client.request(req);
        
        System.out.println("--------------------");
        if (result.isSuccess()) {
            Goods goods = result.getBody();
            System.out.println(goods);
        } else {
            System.out.println("errorMsg:" + result.getMsg());
        }
        System.out.println("--------------------");
    }
    
    @Test
    public void testPing() throws Exception {
        client = new EncryptClient(url, appKey);
        Thread.sleep(30000);
        GoodsParam param = new GoodsParam();
        param.setGoods_name("iphone6");
        GoodsReq req = new GoodsReq("goods.get", param);

        GoodsResp result = client.request(req);
        
        System.out.println(JSON.toJSONString(result));
    }
}
