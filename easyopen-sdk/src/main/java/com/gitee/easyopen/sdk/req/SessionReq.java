package com.gitee.easyopen.sdk.req;

import com.gitee.easyopen.sdk.BaseNoParamReq;
import com.gitee.easyopen.sdk.resp.SessionResp;

public class SessionReq extends BaseNoParamReq<SessionResp> {

    public SessionReq(String name) {
        super(name);
    }


}
