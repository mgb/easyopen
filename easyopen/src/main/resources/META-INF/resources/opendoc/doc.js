String.prototype.startWith=function(str){
	if(!str) {return false;}
	return this.substring(0,str.length) == str;
}  
String.prototype.endWith=function(str){    
	if(!str) {return false;}
	return this.substring(this.length - str.length) == str;       
}

function initTree() {
	$('#tree').ztree_toc({
        _header_nodes: [{
            id: 1,
            pId: 0,
            name: document.title,
            open: false
        }],
        // 第一个节点
        ztreeSetting: {
            view: {
                dblClickExpand: false,
                showLine: true,
                showIcon: false,
                selectedMulti: false
            },
            data: {
                simpleData: {
                    enable: true,
                    idKey: "id",
                    pIdKey: "pId"
                    // rootPId: "0"
                }
            },
            callback: {
                beforeClick: function(treeId, treeNode) {
                    $('a').removeClass('curSelectedNode');
                    if (treeNode.id == 1) {
                        $('body').scrollTop(0);
                    }
                    if ($.fn.ztree_toc.defaults.is_highlight_selected_line == true) {
                        $('#' + treeNode.id).css('color', 'red').fadeOut("slow",
                        function() {
                            $(this).show().css('color', 'black');
                        });
                    }
                }
            }
        },
        is_auto_number: false,
        // 菜单是否显示编号，如果markdown标题没有数字标号可以设为true
        documment_selector: '.markdown-body',
        is_expand_all: true // 菜单全部展开
    });
}

function initEvent() {
	// 代码高亮
    $('.highlight').each(function(i, block) {
        hljs.highlightBlock(block);
    });
    
    // 请求按钮
    $('.api-block').find('.post-btn').click(function() {
        var _parent = $(this).parents('.api-block');
        var name = _parent.find('.api-name').val();
        var version = _parent.find('.api-version').val();

        var $form = _parent.find('form').eq(0);
        var hasFile = $form.find('input[type="file"]').length > 0;
        
        var jsonData = getFormData($form);
        console.log(jsonData)
        
        var _param = _parent.find('.api-data');
        var _result = _parent.find('.api-result');
        var _postResult = _parent.find('.post-result');

        sdk.config({
            url: $('#url').val(),
            app_key: $('#appKey').val(),
            secret: $('#secret').val(),
        });

        sdk.post({
            name: name // 接口名
            ,version:version
            ,access_token:$('#access_token').val()
            ,jwt:$('#jwt').val()
            ,data: jsonData // 请求参数
            ,form:hasFile ? $form.get(0) : null
            ,callback: function(resp, postDataStr) { // 成功回调
            	_param.val(postDataStr);
                _result.val(formatJson(JSON.stringify(resp)));
                _postResult.show();
            }
        });
        return false;
    });
    
    $('.api-item').find('.new-win-btn').click(function(){
    	var $div = $(this).parents('.api-item');
    	
    	window.open(ctx + "/opendoc/new.html?id=" + $div.attr('id'))
    	
    	return false;
    });
    
    var inputId = 0;
    
    $('.api-item').find('.add-array-btn').click(function(){
    	var $btn = $(this);
    	var $td = $btn.parent();
    	var $input = $td.find('input').eq(0);
    	
    	var $newInput = $input.clone();
    	if($newInput.attr('type') == 'file') {
    		$newInput.attr('name','upload_file_' + inputId++)
    	}
    	$newInput.css({'display':'table', 'margin-top':'1px'}).attr('id',$input.attr('id') + inputId++).val('');
    	$td.append($newInput);
    });
    
    $('.api-item').find('.del-array-btn').click(function(){
    	var $btn = $(this);
    	var $td = $btn.parent();
    	var $inputs = $td.find('input');
    	var len = $inputs.length;
    	if(len > 1) {
    		$inputs.eq(len - 1).remove();
    	}
    });
    
    function getFormData($form) {
        var data = {};
        var $table = $form.find('> table').eq(0);
        
        buildData(data, $table);
        
        return data;
    }
    
    function buildData(data, $table) {
    	// 查询这个table下面第一层的td,不包括嵌套table中的td
        var $tds = $table.find('> tbody > tr > td');
        
        $tds.each(function(i, el){
        	var $td = $(el);
        	// 查找td下面的table
        	var $childTdTable = $td.find('>table').eq(0);
        	
        	if($childTdTable.length > 0) {
        		var _parentname = $childTdTable.attr('parentname');
        		var _data = {}
        		data[_parentname] = _data;
        		buildData(_data, $childTdTable);
        	} else {
        		var $els = $td.find('input[type="text"]');
        		if($els.length > 0) {
        			$els.each(function(i, el){
        				var name = el.name;
        				var type = el.type;
        				var elVal = el.value;
        				var dataValue = data[name];
        				
        				if (dataValue) {
        					if ($.isArray(dataValue)) {
        						elVal && dataValue.push(elVal);
        					} else {
        						data[name] = [dataValue, elVal];
        					}
        				} else {
        					var val = $(el).attr('arrinput') ? [elVal] : elVal;
        					data[name] = val;
        				}
        			});
        		}
        		
        	}
        });
    }
}

