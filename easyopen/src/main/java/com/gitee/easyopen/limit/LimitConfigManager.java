package com.gitee.easyopen.limit;

import java.util.List;

/**
 * 限流配置管理
 * 
 * @author tanghc
 */
public interface LimitConfigManager {
    
    /**
     * 从数据库中读取加载到缓存中.服务器启动完成会先加载此方法.
     */
    void loadToLocalCache();

    /**
     * 获取接口的限流配置
     * 
     * @param nameVersion
     *            接口名版本号
     * @return 没有则null
     */
    LimitConfig getApiRateConfig(String nameVersion);

    /**
     * 总数
     * 
     * @param apiSearch
     * @return
     */
    long getTotal(LimitSearch apiSearch);

    /**
     * 获取限流列表
     * 
     * @param apiSearch
     * @return
     */
    List<LimitConfig> listLimitConfig(LimitSearch apiSearch);

    /**
     * 保存配置到数据库
     * @param limitConfig
     * @return
     */
    int save(LimitConfig limitConfig);

}
