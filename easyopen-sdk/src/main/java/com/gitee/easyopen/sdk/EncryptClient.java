package com.gitee.easyopen.sdk;

/**
 * 数据加密传输客户端
 * 
 * @author tanghc
 *
 */
public class EncryptClient extends OpenClient {

    public EncryptClient(String url, String appKey) {
        super(url, appKey, "");
    }

    private EncryptClient(String url, String appKey, String secret) {
        super(url, appKey, secret);
    }

    private EncryptClient(String url, String appKey, String secret, String lang) {
        super(url, appKey, secret, lang);
    }

    @Override
    public RequestMode getRequestMode() {
        return RequestMode.ENCRYPT;
    }

}
