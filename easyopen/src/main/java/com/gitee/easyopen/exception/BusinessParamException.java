package com.gitee.easyopen.exception;

public class BusinessParamException extends ApiException {
    private static final long serialVersionUID = 1L;

    public BusinessParamException(String msg, String code) {
        super(msg, code);
    }

}
